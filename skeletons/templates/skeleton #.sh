#!/bin/sh
# -*- coding: utf-8 -*-
# <+FILE NAME+>
# created: <+DATE+>
# author: <+AUTHOR+> <+EMAIL+>

# directory safety. does not resolve links!
# DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

help () {
    echo -e "usage: $0 [COMMANDS]\n
    possible commands are:\n
    \t-h this help message\n"
}

log () {
    if which logger &>/dev/null; then
        # log to syslog daemon
        logger -s -t "$prog" "$@"
    else
        echo "$(date) ["$prog"] $@" >> "$LOGFILE"
    fi
}

while getopts "h" optname
    do
    case "$optname" in
        "h")
            help
            exit 0
            ;;
        "?")
            echo "unknown option $OPTARG"
            exit 1
            ;;
        ":")
            echo "no argument value for option $OPTARG"
            exit 1
            ;;
        *)
            # should not occur
            echo "unknown error while processing options"
            exit $?
            ;;
    esac
done && shift $((OPTIND-1))

# check for root
# [ "$EUID" -ne 0 ] && log "Please run as root!" && exit 1

<+execute(set ft=sh)+><+CURSOR+>

# vim: tabstop=8 expandtab shiftwidth=4 softtabstop=4 fenc=utf-8
