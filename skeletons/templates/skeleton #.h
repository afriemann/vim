/* <+FILE NAME+>
 * -*- coding: utf-8 -*-
 *
 *
 */

#ifndef <+FILE NAME:us*\W*_*+>
#define <+FILE NAME:us*\W*_*+>

<+execute(set ft=c)+><+CURSOR+>

#endif

// vim: tabstop=8 expandtab shiftwidth=4 softtabstop=4 fenc=utf-8
