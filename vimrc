" Note: Skip initialization for vim-tiny or vim-small.
if 0 | endif

if has('vim_starting')
  if &compatible
    set nocompatible "Be iMproved"
  endif
endif

" =============================================================================
"   Plugins
" =============================================================================
  call plug#begin('~/.vim/plugged')

" General
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
  Plug 'tpope/vim-sensible'
  Plug 'thinca/vim-localrc'
    set secure

  Plug 'ciaranm/securemodelines'
  Plug 'gcavallanti/vim-noscrollbar'
  "  set statusline=%<%f\ %h%m%r%=%-14.(%l,%c%V%)\ %{noscrollbar#statusline(20,'■','◫',['◧'],['◨'])}
    set statusline=%<%f\ %h%m%r%=%-14.(%l,%c%V%)\ %{noscrollbar#statusline()}

" Usability
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
  Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': 'yes \| ./install' }
    nmap <C-p> :FZF -m<cr>
  Plug 'heavenshell/vim-pydocstring'
  Plug 'tpope/vim-surround'
  Plug 'junegunn/vim-easy-align'
    " Start interactive EasyAlign in visual mode (e.g. vip<Enter>)
    vmap <Enter> <Plug>(EasyAlign)
    " Start interactive EasyAlign for a motion/text object (e.g. gaip)
    nmap ga <Plug>(EasyAlign)
  Plug 'terryma/vim-multiple-cursors'
    let g:multi_cursor_use_default_mapping=0
    let g:multi_cursor_start_key='<C-n>'
    let g:multi_cursor_next_key='<C-j>'
    let g:multi_cursor_prev_key='<C-k>'
    let g:multi_cursor_skip_key='<C-x>'
    let g:multi_cursor_quit_key='<Esc>'
  Plug 'tpope/vim-repeat'
  Plug 'rhysd/clever-f.vim'
  Plug 'vim-scripts/ReplaceWithRegister'
  Plug 'christoomey/vim-sort-motion'
  Plug 'wellle/targets.vim'
  Plug 'mhinz/vim-grepper'

                    " tomtom "
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
  Plug 'tomtom/tlib_vim' | Plug 'tomtom/tcomment_vim' | Plug 'tomtom/tskeleton_vim'
    let g:tskelUserName='Aljosha Friemann'
    let g:tskelUserEmail='aljosha.friemann@gmail.com'
    let g:tskelUserStreet='Kranichsteiner Str. 15'
    let g:tskelUserPostalCode='64289'
    let g:tskelUserCity='Darmstadt'
    let g:tskelUserCountry='Germany'

                    " autocompletion "
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
  "Plug 'ajh17/VimCompletesMe'
  "Plug 'davidhalter/jedi-vim', { 'for': 'python' }

                    " Syntax highlighting "
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
  Plug 'vim-scripts/pam.vim', { 'for': 'pamconf' }
  Plug 'chrisbra/csv.vim', { 'for': 'csv' }
  Plug 'tpope/vim-markdown', { 'for': 'markdown' }
  Plug 'rodjek/vim-puppet', { 'for': 'puppet' }
  Plug 'derekwyatt/vim-sbt', { 'for': 'sbt' }
  Plug 'derekwyatt/vim-scala', { 'for': 'scala' }
  Plug 'vim-scripts/promela.vim', { 'for': 'promela' }
  Plug 'PotatoesMaster/i3-vim-syntax', { 'for': 'i3' }
  Plug 'robbles/logstash.vim', { 'for': 'logstash' }
  Plug 'ekalinin/Dockerfile.vim', { 'for': 'Dockerfile' }
  Plug 'rust-lang/rust.vim', { 'for': 'rust' }
  Plug 'vim-scripts/AnsiEsc.vim'
  Plug 'PCGen/vim-pcgen'

                          " Colours "
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
  Plug 'godlygeek/csapprox'
  Plug 'flazz/vim-colorschemes'
  " Plug 'jonathanfilip/vim-lucius'

  call plug#end()

  " Required:
  filetype plugin indent on

" =============================================================================
"   General
" =============================================================================
  scriptencoding utf-8

  set paste

  set cryptmethod=blowfish2

  set history=300

  set autoread "reload changed files automatically"

  set lazyredraw "Don't redraw while executing macros"

  "change vim path"
  set path+=/usr/lib/gcc/**/include
  set path=.,,**,/etc/

  "automatically copy the contents of the + register to the * register"
  set clipboard=unnamed,unnamedplus

  set smartcase ignorecase hlsearch incsearch magic "search options"
  set showmatch mat=2 "Show matching brackets when text indicator is over them"

  " set nobackup "don't write ~ backups"
  set backupdir=/tmp,.
  set directory=/tmp,.

  set undofile undodir=~/.vim/undodir "use an undo file"

  " Tell vim to remember certain things when we exit
  "  '10  :  marks will be remembered for up to 10 previously edited files
  "  "100 :  will save up to 100 lines for each register
  "  :20  :  up to 20 lines of command-line history will be remembered
  "  %    :  saves and restores the buffer list
  "  n... :  where to save the viminfo files
  set viminfo='10,\"100,:20,%,n~/.vim/viminfo

  set wildmenu
  set wildignore=*.o,*~,*.pyc "Ignore compiled files"

  set scrolloff=15 "Keep 15 lines below and above the cursor"

  set noerrorbells visualbell t_vb= tm=500 "no more flashing"

  if has('syntax') && !exists('g:syntax_on')
    syntax enable
  endif

" =============================================================================
" autocomplete / omnicomplete / tags
" =============================================================================
  set omnifunc=syntaxcomplete#Complete

  " inoremap <expr> <CR> pumvisible() ? "\<C-y>" : "\<C-g>u\<CR>"
  " " open omni completion menu closing previous if open and opening new menu without changing the text
  " inoremap <expr> <C-Space> (pumvisible() ? (col('.') > 1 ? '<Esc>i<Right>' : '<Esc>i') : '') .
  "             \ '<C-x><C-o><C-r>=pumvisible() ? "\<lt>C-n>\<lt>C-p>\<lt>Down>" : ""<CR>'
  " " open user completion menu closing previous if open and opening new menu without changing the text
  " inoremap <expr> <S-Space> (pumvisible() ? (col('.') > 1 ? '<Esc>i<Right>' : '<Esc>i') : '') .
  "             \ '<C-x><C-u><C-r>=pumvisible() ? "\<lt>C-n>\<lt>C-p>\<lt>Down>" : ""<CR>'

  " Don't scan includes; tags file is more performant.
  set completeopt=longest,menuone

  set wildmode=full
  "THIS AFFECTS expand() !!!!!!!!!!!!!!!!!!!!
  set wildignore+=*/bin/*,tags,*.o,*.obj,*.dll,*.class,.hg,.svn,*.pyc,*/tmp/*,*/grimoire-remote/*,*.so,*.swp,*.zip,*.exe,*.jar,*/opt/*,*/gwt-unitCache/*,*.cache.html,*.pdf,*.wav,*.mp3,*.ogg

  " Files with these suffixes get a lower priority when matching a wildcard
  set suffixes+=.aux,.log,.dvi,.bbl,.blg,.brf,.cb,.ind,.idx,.ilg,.inx,.out,.toc

" =============================================================================
"   Text formatting
" =============================================================================
  " whitespacecs and indentation
  set expandtab smarttab tabstop=8 shiftwidth=4 softtabstop=4
  set fileformats=unix fileencoding="utf-8"

  " wrap lines at 120 characters
  set formatoptions+=rno1l
  set textwidth=120
  set wrapmargin=0

" =============================================================================
"   Visual
" =============================================================================
  " colorscheme vividchalk
  colorscheme molokai

  " ensure vim can use the whole frame ratpoison allocates to it no screen
  set guiheadroom=0
  set background=dark
  set laststatus=2
  set number ruler
  set synmaxcol=1000 " don't syntax-highlight long lines

  set list
  set listchars=tab:>\ ,trail:-,extends:>,precedes:<,nbsp:+
    syntax match Tab /\t/
    highlight Tab gui=underline guifg=blue ctermbg=blue
    highlight SpecialKey ctermfg=5

  set cursorline cursorcolumn
    highlight CursorLine ctermbg=234 guibg=#1c1c1c
    highlight ColorColumn ctermbg=234 guibg=#1c1c1c
    " show 80 and 120 character mark
    let &colorcolumn=join(range(81,999),",")
    let &colorcolumn="80,".join(range(120,999),",")

  " highlight Pmenu ctermfg=green ctermbg=black guifg=#66D9EF  guibg=#000000

  " use 256 colors in Console mode if we think the terminal supports it
  if &term =~? 'mlterm\|xterm\|256color'
      set t_Co=256
  endif

" =============================================================================
"   Keybindings
" =============================================================================
  " j,k move by screen line instead of file line
  nnoremap j gj
  nnoremap k gk
  xnoremap j gj
  xnoremap k gk

  " Smart way to move between buffers
  map <C-j> <C-W>j
  map <C-k> <C-W>k
  map <C-h> <C-W>h
  map <C-l> <C-W>l

  " underline with anything "
  function! s:Underline(chars)
    let chars = empty(a:chars) ? '-' : a:chars
    let nr_columns = virtcol('$') - 1
    let uline = repeat(chars, (nr_columns / len(chars)) + 1)
    put =strpart(uline, 0, nr_columns)
  endfunction
  command! -nargs=? Underline call s:Underline(<q-args>)
  nnoremap <leader>u  :Underline<space>

  " replace in visual with current buffer content without overwriting buffer
  "vmap r "_dP

  " remove trailing whitespaces
  nnoremap <leader>s :%s/\s\+$//g<CR>

  " remap the fucking F1 key to clearing search highlighting
  noremap! <F1> :noh<CR>
  noremap <F1> :noh<CR>

  " open scratchpad on F2
  map <F2> :TScratch<CR>

  " tabs
  nnoremap <leader>t  :tabnew<space>
  nnoremap <leader>f  :tabfind<space>
  nnoremap <leader>d  :tabclose<CR>
  nnoremap <tab>      :tabnext<CR>
  nnoremap <S-tab>    :tabprevious<CR>

  " Switch CWD to the directory of the open buffer
  map <leader>cd :cd %:p:h<cr>:pwd<cr>

  " execute self
  map <leader>r :w<CR>:!./%

" =============================================================================
" augroups
" =============================================================================

  " Jump to the last position when reopening a file (except Git commit)
  augroup vimrc_general
    autocmd!
    autocmd BufReadPost * if @% !~# '\.git[\/\\]COMMIT_EDITMSG$' && line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g`\"" | endif
  augroup END

 augroup vimrc_filetypes
    autocmd!
    autocmd BufNewFile,BufRead *.txt,README,INSTALL,NEWS,TODO if &ft == ""|set ft=text|endif
    autocmd FileType text setlocal tabstop=4 shiftwidth=4 textwidth=80
  augroup END

  " spellchecking and automatic linebreaks in git commit messages
  augroup vimrc_git
    autocmd!
    autocmd FileType gitconfig setlocal commentstring=#\ %s
    autocmd Filetype gitcommit setlocal spell textwidth=80
    autocmd FileType gitcommit au! BufEnter COMMIT_EDITMSG call setpos('.', [0, 1, 1, 0])
  augroup END

  " python name printed variable
  augroup vimrc_python
    autocmd!
    autocmd Filetype python nnoremap <leader>p :%s/print(\([a-zA-Z0-9_]\+\))/print('\1: %s' % \1)/gc<CR>
    autocmd Filetype python nnoremap <leader>dt :!python -m doctest %<CR>
    autocmd FileType python set omnifunc=pythoncomplete#Complete
  augroup END

  augroup vimrc_vim
    autocmd!
    autocmd Filetype vim let b:vcm_tab_complete = 'vim'
  augroup END

" vim: tabstop=2 expandtab shiftwidth=2 softtabstop=2
